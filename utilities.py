# -*- coding: utf-8 -*-

from __future__ import division, print_function

import numpy as np

from functools import wraps

from scipy.constants import hbar
from scipy.constants import value as value_of

bohr_magneton = value_of("Bohr magneton") * 1e-4 # in J per Gauss

lithium_mass               = 9.9883414e-27 # in kg
lithium_D2_resonance_width = 5.8724e6 # in Hz
lithium_D2_wavelength      = 670.977338e-9 # in m
lithium_D2_wavenumber      = 2 * np.pi / lithium_D2_wavelength # in m^-1
lithium_D2_recoil_velocity = 9.886776e-2 # in m/s


def meters_to_inches(meters):
    inches_per_meter = 39.3701
    return inches_per_meter * meters


def doppler_shift(v):
    return lithium_D2_wavenumber * v


def zeeman_shift(B):
    return B * bohr_magneton / hbar


def generate_smoothing_function(z_0, coil_radius, weights=None, center=True):
    """
    Generates a function to "smooth out" an idealized field profile,
    resulting in a more realistic profile. Conversely, dividing an
    idealized current profile by the output may result in a field
    which is closer to ideal.

    Inputs: z_0:         Length of the slower solenoid, in meters.
            coil_radius: Radius of the solenoid, in meters.
            weights:     2-tuple of values used to weight the two sides
                         (z=0 and z=z_0) of the function. Each weight
                         must be in the range (0, 1).
            center:      Boolean value describing whether to "center"
                         the weights.  If True, weights are normalized
                         such that sum(weights) = 1; otherwise, a
                         constant offset = 1-sum(weights) is added to
                         the smoothing to make up for the difference.

    Returns: Function of one variable (z) describing the smoothing.
    """
    if weights is None:
        weights = np.array([0.5, 0.5])
        offset = 0
    else:
        assert len(weights) == 2
        if center:
            weights = np.array(weights) / np.sum(weights)
            offset = 0
        else:
            weights = np.array(weights)
            assert all(weights < 0.95)
            offset = 1 - np.sum(weights)

    def smoothing_function(z):
        """
        Smoothing function f:
        f(z) = %3.2f [z / (z^2 + r^2)]
             + %3.2f [(z_0 - z) / ((z_0 - z)^2 + r^2)] %s
        where r is the solenoid radius and z_0 is the solenoid length.

        Inputs: z: Distance from the start of the slower, in meters.

        Returns: Unitless smoothing function f(z).
        """
        return (offset +
                weights[0] * (      z) /
                np.sqrt((      z)**2 + coil_radius**2) +
                weights[1] * (z_0 - z) /
                np.sqrt((z_0 - z)**2 + coil_radius**2))

    offset_doc = ("\n             + %3.2f" % offset) if offset != 0 else ""
    smoothing_function.__doc__ %= (weights[0], weights[1], offset_doc)

    return smoothing_function


def scalarize(fun):
    """
    Decorator: allows functions expecting array arguments to be called
    with scalars.
    """
    @wraps(fun)
    def wrapper(arg_array, *args, **kwargs):
        try:
            iter(arg_array)
        except TypeError:
            arg_array = np.array([arg_array])
            is_scalar = True
        else:
            is_scalar = False

        output = fun(arg_array, *args, **kwargs)

        return output[0] if is_scalar else np.array(output)

    return wrapper


def str_to_fun(fun_string, constants=None):
    """
    Converts a stringy function to an actual function.

    Inputs: fun_string: String of the form "50*sqrt(1-z/20)".
                        Can contain constants ("B0*sqrt(1-z/z0)")
                        as long as they are supplied using the
                        *constants* argument.
            constants:  String of the form "B0=50,z0=20" supplying
                        values for each of the constants used in
                        *string*.

    Note: The independant variable must be given as "z".
    """
    if constants:
        for c in constants.split(","):
            constant, value = tuple(c.split("=")) # TODO: error handling
            fun_string = fun_string.replace(constant.strip(), value.strip())

    return eval("lambda z: " + fun_string)


def write_rib_locs_for_scad(fname, coords):
    with open(fname, 'wt') as fh:
        last_i = len(coords) - 1
        for i, (loc, angle) in enumerate(coords):
            line_end = ",\n" if i < last_i else ""
            line = "[%f,%f]%s" % (loc*1000, np.degrees(angle), line_end)
            fh.write(line)

