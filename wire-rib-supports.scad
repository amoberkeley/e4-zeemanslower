// parameters of the coil
wire_diameter = 4.0;
coil_length = 457.2;
wire_radius = wire_diameter / 2;

// extra rib length beyond the coil on either side
extra = 6.0;

// parameters of the support rib
rib_length = coil_length + 2*extra;
rib_width = 9.0;
rib_thickness = 12.0;

// length of the holes; must be greater than rib_width / cos(max_angle)
hole_length = rib_width * 3;

// CHANGE THIS LINE TO CHOOSE AN INPUT FILE
locs_and_angles = [include <rib-locs-280deg.csvish>];

module cutout (loc, angle) {
    translate([loc, 0, 0])
    rotate([90, 0, -angle])
    translate([0, 0, -hole_length/2])
    union() {
        cylinder(r=wire_radius, h=hole_length, $fs=0.01);

        translate([0, -wire_radius, hole_length/2])
        scale([wire_diameter, wire_diameter, hole_length])
        cube(center=true);
    }
}

module cutouts (locs_and_angles) {
    for (x_and_theta = locs_and_angles) {
        x     = x_and_theta[0];
        theta = x_and_theta[1];

        cutout(loc=x, angle=theta);
    }
}

difference() {
    translate([rib_length/2 - extra, 0, rib_thickness/2 - wire_radius])
    scale([rib_length, rib_width, rib_thickness])
    cube(center=true);

    cutouts(locs_and_angles=locs_and_angles);
}
