# -*- coding: utf-8 -*-

from __future__ import division, print_function

import numpy as np
from matplotlib import pyplot as plt

from scipy.integrate import odeint, quad as nintegrate
from scipy.optimize import fmin, minimize_scalar

from scipy.constants import hbar, k as k_B

from utilities import *
from coils import (ProfileGuess, MultilayerCoilOptimizer,
                   VarpitchCoilOptimizer)


def maxwell_pdf_collimated(v, T_oven):
    scale = k_B * T_oven / lithium_mass
    return (0.5 / scale**2) * v**3 * np.exp(-0.5 * v**2 / scale)


class ExperimentalSetup(object):
    """
    Describes a calibrated Zeeman slower.

    Workflow should look like:
    [1] setup = ExperimentalSetup(…)
    [2] ideal_z_0(setup, …, update_setup=True)
    [3] coil_profile = CoilProfile(setup.z_0, setup.coil_radius, …)
    [4] optimizer = CoilOptimizer(coil_profile, setup)
    [5] optimizer.optimize_profile(…, update_setup=True)
    [6] simulate_deceleration(setup, plot=True)
    [7] coil_profile.get_eta(setup, …, plot=True)
    """
    def __init__(self, saturation_parameter=4, z_0=0.4572, v_mot=65,
                 design_parameter=0.65, max_B_bias=-50,
                 coil_radius=0.01905, desired_detuning=None):
        """
        Inputs: saturation_parameter: Saturation parameter S = I/I_sat
                                      of the slowing laser.
                z_0:                  Slower length, in meters. If not
                                      supplied, this is calculated.
                design_parameter:     Design parameter eta.
                v_mot:                Desired velocity at z = z_0, in
                                      m/s.
                max_B_bias:           Maximum allowed bias field, such
                                      that B_bias <= max_B_bias < 0.
                coil_radius:          Radius of the solenoid used to
                                      generate the field, in meters.
                desired_detuning:     Desired laser detuning, in Hz,
                                      such that ideal < desired < 0 Hz.
        """
        self._saturation_parameter = saturation_parameter
        self._design_parameter     = design_parameter
        self._z_0                  = z_0
        self._v_mot                = v_mot
        self._max_B_bias           = max_B_bias
        self._coil_radius          = coil_radius
        self._desired_detuning     = desired_detuning

        self._B_taper = None
        self._B_bias  = None

        self._v_max          = None
        self._v_out          = None
        self._B_of_z         = None
        self._a_max          = None
        self._laser_detuning = None

        self.update_settings()

    @property
    def saturation_parameter(self):
        return self._saturation_parameter

    @saturation_parameter.setter
    def saturation_parameter(self, value):
        self._saturation_parameter = value
        self.update_settings()

    @property
    def design_parameter(self):
        return self._design_parameter

    @design_parameter.setter
    def design_parameter(self, value):
        self._design_parameter = value
        self.update_settings()

    @property
    def z_0(self):
        return self._z_0

    @z_0.setter
    def z_0(self, value):
        self._z_0 = value
        self.update_settings()

    @property
    def v_mot(self):
        return self._v_mot

    @v_mot.setter
    def v_mot(self, value):
        self._v_mot = value
        self.update_settings()

    @property
    def max_B_bias(self):
        return self._max_B_bias

    @max_B_bias.setter
    def max_B_bias(self, value):
        self._max_B_bias = value
        self.update_settings()

    @property
    def coil_radius(self):
        return self._coil_radius

    @coil_radius.setter
    def coil_radius(self, value):
        self._coil_radius = value
        self.update_settings()

    @property
    def desired_detuning(self):
        return self._desired_detuning

    @desired_detuning.setter
    def desired_detuning(self, value):
        self._desired_detuning = value
        self.update_settings()

    @property
    def B_of_z(self):
        return self._B_of_z

    @B_of_z.setter
    def B_of_z(self, value):
        self._B_of_z = value
        self._update_velocity_limits()

    @property
    def laser_detuning(self):
        return self._laser_detuning

    @property
    def a_max(self):
        return self._a_max

    @property
    def v_max(self):
        return self._v_max

    @property
    def v_out(self):
        return self._v_out

    @property
    def B_bias(self):
        return self._B_bias

    @property
    def B_taper(self):
        return self._B_taper

    def update_settings(self, **kwargs):
        """
        Updates the parameters of the setup. For accepted arguments,
        see the __init__ docstring.
        """
        allowed_settings = ("saturation_parameter", "design_parameter",
                            "z_0", "v_mot", "coil_radius", "max_B_bias")
        for k, v in kwargs.iteritems():
            if k in allowed_settings:
                setattr(self, "_"+k, v)
            else:
                raise AttributeError("'%s' is not a supported setting." % k)

        self._B_taper = self._taper_field()
        self._a_max   = self._max_acceleration()

        self._optimize_settings()

    __call__ = update_settings

    def __repr__(self):
        description = (u"ExperimentalSetup object with the following "
                       u"restrictions:\n\n"
                       u"\tsaturation parameter (s) = %2.1f\n"
                       u"\tslower length (z₀)       = %3.1f cm\n"
                       u"\tdesign parameter (η)     = %3.2f\n"
                       u"\tMOT trapping velocity    = %3d m/s\n\n"
                       u"This results in the parameters\n\n"
                       u"\tlaser detuning (Δ)       = %d MHz\n"
                       u"\tBias magnetic field      = %d Gauss\n"
                       u"\tTaper magnetic field     = %d Gauss\n"
                       u"\tmax. capture velocity    = %3d m/s\n"
                       u"\tslowed output velocity   = %3d m/s\n"
                      ) % ( self._saturation_parameter
                          , self._z_0                  * 1e2
                          , self._design_parameter
                          , self._v_mot
                          , self._laser_detuning       * 1e-6
                          , self._B_bias
                          , self._B_taper
                          , self._v_max
                          , self._v_out)

        return description.encode("utf8")

    def _min_B(self, B_of_z):
        return fmin(B_of_z, self._z_0, disp=False, full_output=True)[1]

    def _max_B(self, B_of_z):
        return -fmin(lambda z: -B_of_z(z), 0,
                     disp=False, full_output=True)[1]

    def _optimize_settings(self):
        """
        Finds the values of B_bias and laser_detuning that maximize
        v_max subject to v_mot remaining near the desired value.
        """
        generate_field = self._generate_realistic_field

        def shift_difference_squared(B_bias):
            B_of_z = generate_field(B_bias, self._B_taper, self._z_0,
                                    self._coil_radius)
            min_field = self._min_B(B_of_z)
            laser_detuning = self._ideal_detuning(B_bias)
            min_zeeman_shift = zeeman_shift(min_field) - laser_detuning

            return (min_zeeman_shift - doppler_shift(self._v_mot))**2

        B_bias_guess = -(2 * hbar * lithium_D2_wavenumber /
                         bohr_magneton * self._v_mot)
        result = minimize_scalar(shift_difference_squared,
                                 (2*B_bias_guess, self._max_B_bias))

        if result.success:
            B_bias_ideal = result.x
        else:
            raise RuntimeWarning(result.message)

        ideal_detuning = self._ideal_detuning(B_bias_ideal)
        if self._desired_detuning is not None:
            # these don't work well for desired_detuning < ideal_detuning
            self._B_bias         = ((self._desired_detuning - ideal_detuning) *
                                    hbar / bohr_magneton) + B_bias_ideal
            self._laser_detuning = self._desired_detuning
        else:
            self._B_bias         = B_bias_ideal
            self._laser_detuning = self._ideal_detuning(self._B_bias)

        self._B_of_z = generate_field(self._B_bias, self._B_taper, self._z_0,
                                      self._coil_radius)

        self._update_velocity_limits()

    def _update_velocity_limits(self):
        self._v_max = (zeeman_shift(self._max_B(self._B_of_z)) -
                       self._laser_detuning) / lithium_D2_wavenumber
        self._v_out = (zeeman_shift(self._min_B(self._B_of_z)) -
                       self._laser_detuning) / lithium_D2_wavenumber

    @staticmethod
    def _generate_ideal_field(B_bias, B_taper, z_0, *args, **kwargs):
        """
        Generates an ideal field profile of the form:
        B_ideal(z) = B_bias + B_taper * sqrt(1 - z / z_0)
        """
        @np.vectorize
        def B_of_z(z):
            """
            Ideal field profile of the form:
            B_ideal(z) = B_bias + B_taper * sqrt(1 - z / z_0)

            Inputs: z: Distance from the start of the slower, in meters.

            Returns: The field B(z), in Gauss.
            """
            if 0 <= z <= z_0:
                return B_bias + B_taper * np.sqrt(1 - z / z_0)
            else:
                return 0

        return B_of_z

    @staticmethod
    def _generate_augmented_field(B_bias, B_taper, z_0, coil_radius,
                                  *args, **kwargs):
        """
        Generates an ideal field (for 0 < z < z_0) augmented by a
        1 / r dropoff beyond these points.
        """
        R_squared = coil_radius**2

        @scalarize
        def B_of_z(z):
            """
            Ideal field profile of the form:
            B_ideal(z) = B_bias + B_taper * sqrt(1 - z / z_0)
            for 0 < z < z_0, augmented by an ideal ~ 1 / r dropoff
            beyond these points.

            Inputs: z: Distance from the start of the slower, in meters.

            Returns: The field B(z), in Gauss.
            """
            z = np.array(z)
            B = np.zeros(len(z))

            region_1 =  z <  0
            region_2 = (z >= 0) & (z <= z_0)
            region_3 =             z >  z_0

            B[region_1] = ((R_squared / (R_squared +
                                         z[region_1]**2))**1.5 *
                           (B_bias + B_taper))
            B[region_2] = B_bias + B_taper * np.sqrt(1 - z[region_2] / z_0)
            B[region_3] = ((R_squared / (R_squared +
                                         (z[region_3] - z_0)**2))**1.5 *
                           (B_bias))

            return B

        return B_of_z

    @staticmethod
    def _generate_realistic_field(B_bias, B_taper, z_0, coil_radius,
                                  *args, **kwargs):
        """
        Generates a more realistic field profile of the form:
        B_realistic(z) = B_ideal(z) * [smoothing function]
        """
        B_of_z_ideal = ExperimentalSetup._generate_ideal_field(B_bias, B_taper,
                                                               z_0)

        def make_B_of_z(z_0):
            smoothing_function = generate_smoothing_function(z_0, coil_radius)

            @np.vectorize
            def B_of_z(z):
                """
                Realistic field profile of the form:
                B_realistic(z) = B_ideal(z) * [smoothing function]

                Inputs: z: Distance from the start of the slower,
                           in meters.

                Returns: The field B(z), in Gauss.
                """
                z_effective = 0 if z < 0 else z if z < z_0 else z_0
                return B_of_z_ideal(z_effective) * smoothing_function(z)

            return B_of_z

        B_of_z_initial = make_B_of_z(z_0)

        if B_bias > 0:
            return B_of_z_initial

        def find_inflection(dz=1e-4):
            z_pp = z_0
            z_p  = z_pp - dz
            z    = z_p  - dz
            while z > 0.7 * z_0:
                B_of_z_p = B_of_z_initial(z_p)
                dB1 = B_of_z_initial(z_pp) - B_of_z_p
                dB2 = B_of_z_p - B_of_z_initial(z)

                if dB2 < dB1:
                    return z_p

                z_pp = z_p
                z_p  = z
                z -= dz

            return z_0

        z_inflection = find_inflection()
        B_of_z = make_B_of_z(z_inflection)

        return B_of_z

    @staticmethod
    def _ideal_detuning(B_bias):
        ideal_detuning = bohr_magneton * B_bias / hbar
        correction = 0.96 if ideal_detuning < 0 else 1.2 # emperical
        return correction * ideal_detuning

    def _taper_field(self):
        return ((hbar * lithium_D2_wavenumber / bohr_magneton) *
                np.sqrt(self.design_parameter * self._z_0 *
                        (hbar * lithium_D2_wavenumber *
                         lithium_D2_resonance_width / lithium_mass) *
                        (self._saturation_parameter /
                         (1 + self._saturation_parameter)) -
                        self._v_mot**2))

    def _max_acceleration(self):
        return (hbar * lithium_D2_wavenumber *
                lithium_D2_resonance_width /
                (2 * lithium_mass) *
                self._saturation_parameter /
                (1 + self._saturation_parameter))


def find_v_versus_z(experimental_setup, v_in, r_in, Tr_in, steps):
    """
    Simulates the deceleration process, giving profiles of v (the
    mean axial velocity) and r (the transverse radius) with respect to
    z (the axial position).

    Inputs: experimental_setup: ExperimentalSetup object describing
                                the Zeeman slower which is to be
                                simulated.
            v_in:               Initial mean axial velocities of the
                                particles being simulated, in m/s.
            r_in:               Initial transverse radius of the beam
                                of particles, in meters.
            Tr_in:              Initial transverse temperature of the
                                beam of particles, in Kelvins.
            steps:              Approximate number of simulation steps
                                to take. The true number of steps taken
                                may vary within an order of magnitude.

    Returns: z: List containing the axial location of the particle at
                each timestep.
             v: List containing the axial velocity of the particle at
                each timestep.
             r: List containing the transverse radius of the particle
                at each timestep.

             These values are packed into a 3-tuple of the form
             (z, v, r).
    """
    def acceleration(z, v):
        return -(hbar * lithium_D2_wavenumber * lithium_D2_resonance_width /
                 (2 * lithium_mass) *
                 experimental_setup.saturation_parameter /
                 (1 + experimental_setup.saturation_parameter +
                  (4 / lithium_D2_resonance_width**2) *
                  (experimental_setup.laser_detuning +
                   lithium_D2_wavenumber * v -
                   (bohr_magneton / hbar) *
                   experimental_setup.B_of_z(z))**2) *
                 experimental_setup.design_parameter)

    t_max = 10 * experimental_setup.z_0 / v_in
    timestep = t_max / steps

    def derivatives(z_v_r_n, _):
        z, v, r, n = z_v_r_n

        dz_by_dt = v
        dv_by_dt = acceleration(z, v) if v > 0 else 0

        dn_by_dt = (lithium_mass / (hbar * lithium_D2_wavenumber) *
                    np.abs(dv_by_dt))

        dr_by_dt  = vr_in + (hbar * lithium_D2_wavenumber / lithium_mass *
                             np.sqrt(n / 3))

        return [dz_by_dt, dv_by_dt, dr_by_dt, dn_by_dt]

    vr_in = np.sqrt(2 * k_B * Tr_in / lithium_mass)

    t = np.linspace(0, t_max, steps)
    output = odeint(derivatives, [-0.1, v_in, r_in, 0], t)
    z = output[:,0]
    v = output[:,1]
    r = output[:,2]

    return (z, v, r)


def simulate_deceleration(experimental_setup, v_in=None, r_in=0.002,
                          T_oven=500, d_mot=0.1, steps=1000, plot=True):
    """
    Simulates the deceleration process.

    If plot=True, plots profiles of the Doppler shift of each particle
    (one for each item in v_in) versus z, along with the profile of the
    Zeeman shift corresponding to experimental_setup.

    Gives the final axial speed v_out and transverse radius r_out of
    each particle.

    Inputs: experimental_setup: ExperimentalSetup object describing
                                the Zeeman slower which is to be
                                simulated.
            v_in:               Initial mean axial velocities of the
                                particles being simulated, in m/s.
                                If this is given as a list, a
                                separate simulation is run for each
                                value.
            r_in:               Initial transverse radius of the beam
                                of particles, in meters.
            T_oven:             Temperature of the oven, in Kelvins.
            d_mot:              Distance from the end of the slower to
                                the MOT, in meters.
            steps:              Approximate number of simulation steps
                                to take. The true number of steps taken
                                may vary within an order of magnitude.
            plot:               Boolean value describing whether or not
                                plots should be displayed of the
                                deceleration process.

    Returns: Dictionary indexed by items in v_in, where each entry
             is of the form {v_in: (v_out, r_out)}.
    """
    if v_in is None:
        v_in = np.arange(round(experimental_setup.v_max-91, -1),
                         round(experimental_setup.v_max+41, -1), 30)
    else:
        try:
            iter(v_in)
        except TypeError:
            v_in = [v_in]

    v_and_r_out = {v : (v, r_in) for v in v_in}

    if plot:
        fig, ax_all = plt.subplots(2, sharex=True, figsize=(8, 8))
        ax_field = ax_all[0]
        ax_velocities = ax_field.twinx()
        ax_spreading = ax_all[1]
        max_r_out = 0

    z_out = experimental_setup.z_0 + d_mot
    Tr_in = T_oven * 1e-6 # TODO: why?
    for v_in_val in v_in:
        z, v, r = find_v_versus_z(experimental_setup, v_in_val,
                                  r_in, Tr_in, steps)

        try:
            i_out = next(i for i, z_val in enumerate(z) if z_val > z_out)
        except StopIteration:
            i_out = -1

        if z[i_out] < z_out:
            raise Warning("Not enough time steps!")

        v_and_r_out[v_in_val] = (v[i_out], r[i_out])

        if plot:
            ax_velocities.plot(z, v)
            ax_spreading.plot(z, r*1e3)
            max_r_out = np.maximum(max_r_out, r[i_out])

    if plot:
        B_offset = experimental_setup.laser_detuning * hbar / bohr_magneton

        ax_field.plot(z, experimental_setup.B_of_z(z)-B_offset, "--k")

        ideal_field = experimental_setup._generate_ideal_field(
                        experimental_setup.B_bias,
                        experimental_setup.B_taper,
                        experimental_setup.z_0)
        ax_field.plot(z, ideal_field(z)-B_offset, "--r")

        def label_to_float(label):
            return float(label.get_text().replace(u"\u2212", "-"))

        def float_to_label(number):
            return str(int(np.rint(number))).replace("-", u"\u2212")

        ax_field.set_ylabel(u"axial magnetic field (Gauss)")
        fig.canvas.draw()
        yticks = [round(tick+B_offset, -1) - B_offset
                  for tick in ax_field.get_yticks()]
        ax_field.set_yticks(yticks)
        fig.canvas.draw()
        yticklabels = [float_to_label(label_to_float(label) + B_offset)
                       if len(label.get_text()) > 0
                       else label
                       for label in ax_field.get_yticklabels()]
        ax_field.set_yticklabels(yticklabels)
        ax_field.set_xlim([-0.1, experimental_setup.z_0+0.1])
        ax_field.plot(ax_field.get_xlim(), [-B_offset] * 2, ":k")

        ax_velocities.set_ylim((ax_velocities.get_ylim()[0] - 10,
                                ax_velocities.get_ylim()[1] + 20))
        ax_field.set_ylim((doppler_shift(v) * hbar / bohr_magneton
                           for v in ax_velocities.get_ylim()))
        ax_velocities.set_ylabel(u"v (m / s)")
        ax_spreading.set_xlabel(u"z (m)")
        ax_spreading.set_ylabel(u"transverse radius (mm)")
        ax_spreading.set_ylim([r[0]*1e3-0.5, max_r_out*1e3+0.5])

        boltzmann_v = np.linspace(*tuple(ax_velocities.get_ylim()), num=200)
        boltzmann_pdf = maxwell_pdf_collimated(boltzmann_v, T_oven)
        pdf_to_plot = (ax_field.get_xlim()[1] -
                         0.4 * boltzmann_pdf / np.max(boltzmann_pdf))
        ax_velocities.plot(pdf_to_plot, boltzmann_v, ":r")

    return v_and_r_out


def ideal_z_0(experimental_setup, T_oven, r_in, r_cap, d_mot,
              nozzle_aspect, verbose=False, update_setup=False):
    """
    Finds the ideal slower length z_0 to capture as many atoms as
    possible, given the experimental parameters.

    Inputs: experimental_setup: ExperimentalSetup object describing the
                                parameters of the experiment.
            T_oven:             Temperature of the atom source, in
                                Kelvins.
            r_in:               Radius of the initial atom beam, in
                                meters.
            r_cap:              Maximum beam radius that can be
                                captured by the MOT, in meters.
            d_mot:              Distance from the end of the slower
                                coils to the MOT, in meters.
            nozzle_aspect:      Aspect ratio of the oven nozzle.
            verbose:            Boolean value describing whether or not
                                to show verbose output, including a
                                plot of the captured atom fraction
                                versus z_0.
            update_setup:       Boolean value; if true, the given
                                ExperimentalSetup is updated with the
                                ideal z_0.

    returns: z_0_ideal: The ideal slower length, in meters.
             flux_out:  The fraction of atom flux entering the slower
                        which is captured successfully by the MOT.
    """
    v_mean = 3 * np.sqrt(np.pi / 8) * np.sqrt(k_B * T_oven / lithium_mass)
    vr_in = v_mean * nozzle_aspect
    def r_out(v_0, z_0):
        v_mot = experimental_setup.v_out
        if v_0 > v_mot:
            hbar_k_over_m = hbar * lithium_D2_wavenumber / lithium_mass
            bs = (experimental_setup.design_parameter * z_0 * hbar_k_over_m *
                  lithium_D2_resonance_width *
                  (experimental_setup.saturation_parameter /
                   (1.0 + experimental_setup.saturation_parameter)) -
                  v_mot**2)
            v = v_0 - v_mot
            zvmax = z_0 * (1.0 - v**2 / bs)
            sqrt_term = np.sqrt(hbar_k_over_m / 3.0 * v)
            return (r_in + (vr_in / v_0 * zvmax) +
                    (d_mot / v_mot * (vr_in + sqrt_term)) +
                    (2.0 * z_0 / bs * (vr_in + 2.0 * sqrt_term / 3.0) * v))
        else:
            return r_in + (vr_in / v_0 * (z_0 + d_mot))

    @np.vectorize
    def flux_out(z_0):
        def weighted_maxwell(v):
            pdf = maxwell_pdf_collimated(v, T_oven)
            r_scale = np.minimum(1.0, r_cap / r_out(v, z_0))
            return pdf * r_scale

        return nintegrate(weighted_maxwell, 0, experimental_setup.v_max)[0]

    if verbose:
        plt.figure()
        z_0_plot = np.linspace(0.001, 1.201, 200)
        plt.plot(z_0_plot, flux_out(z_0_plot))
        ax = plt.gca()
        ax.set_title(u"T_oven = %d, r_in = %dmm, r_cap = %dmm, d_mot = %dcm" %
                     (T_oven, r_in*1e3, r_cap*1e3, d_mot*1e2))
        ax.set_ylabel(u"fraction of atom flux trappable by MOT")
        ax.set_xlabel(u"slower length (m)")

    z_0_ideal = fmin(lambda z_0: -flux_out(z_0)**2, 0.2, disp=False)[0]

    if update_setup:
        experimental_setup.z_0 = float(z_0_ideal)

    return (z_0_ideal, flux_out(z_0_ideal))
